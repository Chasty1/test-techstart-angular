export interface Manufacturer {
  _id: string;
  Id: string;
  Name: string;
  Address: string;
  Contact: string;
  __v: number;
}
