export interface UniqueInvoice {
  _id: string;
  docs: {
    Id: string;
    PurchaseDate: string;
  };
}
