import { IProduct } from './product';

export interface IInvoiceLine {
  _id: string;
  Qty: string;
  Weight: Date;
  UnitOfMeasure: number;
  UnitPrice: number;
  ProductId: IProduct;
}
