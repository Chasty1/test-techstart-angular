export interface Distributor {
  _id: string;
  Id: string;
  Name: string;
  Address: string;
  Contact: string;
  __v: number;
}
