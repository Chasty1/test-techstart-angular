import { Customer } from './customer';
import { Distributor } from './distributor';
import { IInvoiceLine } from './invoice-line';
import { IProduct } from './product';
import { Manufacturer } from './manufacturer';

export interface IInvoice {
  Id: string;
  InvoiceNumber: string;
  PurchaseDate: string;
  CustomerLocationId: Customer;
  DistributorLocationId: Distributor;
  Qty: Number;
  Weight: Number;
  UnitOfMeasure: String;
  UnitPrice: Number;
  ProductId: IProduct;
  ManufacturerId: Manufacturer;
  TotalPrice: Number;
}
