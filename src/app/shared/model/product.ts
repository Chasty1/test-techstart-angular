export interface IProduct {
  _id: string;
  Id: string;
  Description: string;
  ProductCode: string;
  DistributorLocationId: string;
  ManufacturerId: string;
}
