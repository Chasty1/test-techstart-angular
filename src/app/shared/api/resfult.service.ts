import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Configuracion } from './config';
import { Observable } from 'rxjs';
import { IInvoice } from '../model/invoice';
import { Distributor } from '../model/distributor';
import { IProduct } from '../model/product';
import { Customer } from '../model/customer';
import { UniqueInvoice } from '../model/unique-invoice';

@Injectable({
  providedIn: 'root',
})
export class RestfulService {
  private actionUrl: string;
  //private headers: Headers;
  constructor(private http: HttpClient, private configuration: Configuracion) {
    this.actionUrl =
      '' +
      // + configuration.Server
      // configuration.ApiUrl;
      'http://localhost:3000/api/';
    console.log(configuration.ApiUrl);
  }

  public getInvoices = (): Observable<IInvoice[]> => {
    const body = new HttpParams();

    return this.http.get<IInvoice[]>(this.actionUrl + 'invoice/', {
      headers: new HttpHeaders().set(
        'Content-Type',
        'application/x-www-form-urlencoded'
      ),
    });
  };

  public getDistributors = (): Observable<Distributor[]> => {
    const body = new HttpParams();

    return this.http.get<Distributor[]>(this.actionUrl + 'distributor/', {
      headers: new HttpHeaders().set(
        'Content-Type',
        'application/x-www-form-urlencoded'
      ),
    });
  };

  public getProducts = (): Observable<IProduct[]> => {
    const body = new HttpParams();

    return this.http.get<IProduct[]>(this.actionUrl + 'product/', {
      headers: new HttpHeaders().set(
        'Content-Type',
        'application/x-www-form-urlencoded'
      ),
    });
  };

  public getCustomers = (): Observable<Customer[]> => {
    const body = new HttpParams();

    return this.http.get<Customer[]>(this.actionUrl + 'customer/', {
      headers: new HttpHeaders().set(
        'Content-Type',
        'application/x-www-form-urlencoded'
      ),
    });
  };

  public getUniqueInvoices = (): Observable<UniqueInvoice[]> => {
    const body = new HttpParams();

    return this.http.get<UniqueInvoice[]>(
      this.actionUrl + 'invoice/getDistinticInvoices',
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        ),
      }
    );
  };

  public PostInvoiceByProduct = (productId: string): Observable<IInvoice[]> => {
    const body = new HttpParams().set('ProductId', productId);

    return this.http.post<IInvoice[]>(
      this.actionUrl + 'invoice/getByProduct',
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        ),
      }
    );
  };

  public PostInvoiceByCustomer = (
    customerId: string
  ): Observable<IInvoice[]> => {
    const body = new HttpParams().set('CustomerId', customerId);

    return this.http.post<IInvoice[]>(
      this.actionUrl + 'invoice/getByCustomer',
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        ),
      }
    );
  };

  public PostInvoiceByDistributor = (
    distributorId: string
  ): Observable<IInvoice[]> => {
    const body = new HttpParams().set('DistributorId', distributorId);

    return this.http.post<IInvoice[]>(
      this.actionUrl + 'invoice/getByDistributor',
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        ),
      }
    );
  };

  public PostInvoicesByInvoice = (
    invoiceNumber: string
  ): Observable<IInvoice[]> => {
    const body = new HttpParams().set('InvoiceNumber', invoiceNumber);

    return this.http.post<IInvoice[]>(
      this.actionUrl + 'invoice/getByInvoice',
      body.toString(),
      {
        headers: new HttpHeaders().set(
          'Content-Type',
          'application/x-www-form-urlencoded'
        ),
      }
    );
  };
}
