import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { RestfulService } from './shared/api/resfult.service';
import { Customer } from './shared/model/customer';
import { Distributor } from './shared/model/distributor';
import { IInvoice } from './shared/model/invoice';
import { IProduct } from './shared/model/product';
import { UniqueInvoice } from './shared/model/unique-invoice';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  filtro_nombre = '';
  cshort_code_tour = '';
  dataSource: MatTableDataSource<IInvoice> = new MatTableDataSource();
  columnsToDisplay = [
    'InvoiceNumber',
    'PurchaseDate',
    'DistributorLocationId',
    'DistributorLocationId2',
    'CustomerLocationId',
    'CustomerLocationId2',
    'ManufacturerId',
    'ManufacturerId2',
    'ProductId',
    'ProductId2',
    'UnitOfMeasure',
    'Qty',
    'Weight',
    'UPrice',
    'Total',
  ];

  invoice = '';

  distributors: Distributor[] = [];
  customers: Customer[] = [];
  products: IProduct[] = [];
  uniqueInvoices: UniqueInvoice[] = [];

  selectedGroupedBy: string = 'All';
  groups: string[] = ['All', 'Invoice', 'Distributor', 'Customer', 'Product'];

  filterByProduct(id: string) {
    this.datasource.PostInvoiceByProduct(id).subscribe((data: IInvoice[]) => {
      this.dataSource = new MatTableDataSource(data);
    });
  }

  radioChange(e: any) {
    if (e === 'All') {
      this.getAllInvoices();
    }
  }

  filterByInvoice(id: string) {
    this.datasource.PostInvoicesByInvoice(id).subscribe((data: IInvoice[]) => {
      this.dataSource = new MatTableDataSource(data);
    });
  }

  filterByCustomer(id: string) {
    this.datasource.PostInvoiceByCustomer(id).subscribe((data: IInvoice[]) => {
      this.dataSource = new MatTableDataSource(data);
    });
  }

  filterByDistributor(id: string) {
    this.datasource
      .PostInvoiceByDistributor(id)
      .subscribe((data: IInvoice[]) => {
        this.dataSource = new MatTableDataSource(data);
      });
  }

  onCloseSelect() {}

  constructor(private datasource: RestfulService) {}

  ngOnInit() {
    this.getAllInvoices();
    this.getDistinctInvoices();
    this.getAllDistributors();
    this.getAllProducts();
    this.getAllCustomers();
  }

  getAllInvoices(): void {
    this.datasource.getInvoices().subscribe((data: IInvoice[]) => {
      this.dataSource = new MatTableDataSource(data);
    });
  }

  getDistinctInvoices(): void {
    this.datasource.getUniqueInvoices().subscribe((data: UniqueInvoice[]) => {
      this.uniqueInvoices = data;
    });
  }

  getAllCustomers(): void {
    this.datasource.getCustomers().subscribe((data: Customer[]) => {
      this.customers = data;
    });
  }

  getAllProducts(): void {
    this.datasource.getProducts().subscribe((data: IProduct[]) => {
      this.products = data;
    });
  }

  getAllDistributors(): void {
    this.datasource.getDistributors().subscribe((data: Distributor[]) => {
      this.distributors = data;
    });
  }
}
